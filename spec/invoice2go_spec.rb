require 'spec_helper'

RSpec.describe Invoice2go do
  after do
    Invoice2go.reset
  end

  describe ".client" do
    it 'is an Invoice2go::Client' do
      expect(Invoice2go.client).to be_a(Invoice2go::Client)
    end
  end

  describe '#api_key' do
    it 'returns the default api key' do
      expect(Invoice2go.api_key).to eq(Invoice2go::Configuration::DEFAULT_API_KEY)
    end
  end

  describe '#api_key=' do
    it 'sets the api key' do
      Invoice2go.api_key = 'test'
      expect(Invoice2go.api_key).to eq('test')
    end
  end

  describe '#api_version' do
    it 'returns the default api version' do
      expect(Invoice2go.api_version).to eq(Invoice2go::Configuration::DEFAULT_API_VERSION)
    end
  end

  describe '#api_version=' do
    it 'sets the api_version' do
      Invoice2go.api_version = '/test'
      expect(Invoice2go.api_version).to eq('/test')
    end
  end

  describe '#adapter' do
    it 'returns the default adapter' do
      expect(Invoice2go.adapter).to eq(Invoice2go::Configuration::DEFAULT_ADAPTER)
    end
  end

  describe '#adapter=' do
    it 'sets the adapter' do
      Invoice2go.adapter = :typhoeus
      expect(Invoice2go.adapter).to eq(:typhoeus)
    end
  end

  describe '#endpoint' do
    it 'returns the default endpoint' do
      expect(Invoice2go.endpoint).to eq(Invoice2go::Configuration::DEFAULT_ENDPOINT)
    end
  end

  describe '#endpoint=' do
    it 'sets the endpoint' do
      Invoice2go.endpoint = 'http://www.google.com'
      expect(Invoice2go.endpoint).to eq('http://www.google.com')
    end
  end

  describe '#configure' do
    Invoice2go::Configuration::VALID_OPTIONS_KEYS.each do |key|

      it "set the #{key}" do
        Invoice2go.configure do |config|
          config.send("#{key}=", key)
          expect(Invoice2go.send(key)).to eq(key)
        end
      end
    end
  end
end