require 'spec_helper'

RSpec.describe Invoice2go::Client do

  it 'connects using the configured endpoint and api version' do
    client = Invoice2go::Client.new
    endpoint = URI.parse("#{client.endpoint}#{client.api_version}/")
    connection = client.send(:connection).build_url(nil).to_s
    expect(connection).to eq(endpoint.to_s)
  end

end
