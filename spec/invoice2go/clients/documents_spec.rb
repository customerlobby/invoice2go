require 'spec_helper'

RSpec.describe Invoice2go::Client::Documents do

  before do
    @client = Invoice2go::Client.new
    @test_account_id = 1
    @test_document_id = 2
  end

  describe '#documents' do

    before do
      stub_get("accounts/#{@test_account_id}/documents").to_return(body: fixture('documents_list.json'),
                                                 headers: {content_type: "application/json; charset=utf-8",
                                                           authorization: 'Basic blah'})
    end

    it 'returns a list of documents' do
      @client.documents(@test_account_id)
      expect(a_get("accounts/#{@test_account_id}/documents")).to have_been_made
    end
  end

  describe "#document" do

    before do
      stub_get("accounts/#{@test_account_id}/documents/#{@test_document_id}").to_return(body: fixture('document.json'),
                                                         headers: {content_type: "application/json; charset=utf-8",
                                                                   authorization: 'Basic blah'})
    end

    it 'returns a single account that matches the given id' do
      @client.document(@test_account_id, @test_document_id)
      expect(a_get("accounts/#{@test_account_id}/documents/#{@test_document_id}")).to have_been_made
    end
  end

end