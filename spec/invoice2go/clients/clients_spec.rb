require 'spec_helper'

RSpec.describe Invoice2go::Client::Clients do

  before do
    @client = Invoice2go::Client.new
    @test_account_id = 1
  end

  describe '#clients' do

   before do
     stub_get("accounts/#{@test_account_id}/clients").to_return( body: fixture('clients_list.json'),
                                               headers: { content_type: "application/json; charset=utf-8",
                                                          authorization: 'Basic blah'})
   end

   it 'returns a list of clients for the given account id' do
     @client.clients(@test_account_id)
     expect(a_get("accounts/#{@test_account_id}/clients")).to have_been_made
   end
  end

end
