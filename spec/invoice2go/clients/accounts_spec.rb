require 'spec_helper'

RSpec.describe Invoice2go::Client::Accounts do

  before do
    @client = Invoice2go::Client.new
    @test_account_id = 1
  end

  describe '#accounts' do

   before do
     stub_get("accounts").to_return(body: fixture('accounts_list.json'),
                                    headers: { content_type: "application/json; charset=utf-8",
                                               authorization: 'Basic blah'})
   end

   it 'returns a list of accounts' do
     @client.accounts
     expect(a_get("accounts")).to have_been_made
   end
  end

  describe "#account" do

    before do
      stub_get("accounts/#{@test_account_id}").to_return(body: fixture('account.json'),
                                       headers: {content_type: "application/json; charset=utf-8",
                                                 authorization: 'Basic blah'})
    end

    it 'returns a single account that matches the given id' do
      @client.account(@test_account_id)
      expect(a_get("accounts/#{@test_account_id}")).to have_been_made
    end
  end

end
