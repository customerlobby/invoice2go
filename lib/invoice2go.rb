require 'faraday'
require 'faraday_middleware'
require 'active_support/all'
require 'invoice2go/version'

Dir[File.expand_path('../../faraday/*.rb', __FILE__)].each{|f| require f}
require File.expand_path('../invoice2go/configuration', __FILE__)
require File.expand_path('../invoice2go/api', __FILE__)
require File.expand_path('../invoice2go/client', __FILE__)

module Invoice2go

  extend Configuration
  # Alias for Invoice2go::Client.new
  # @return [Invoice2go::Client]
  def self.client(options = {})
    Invoice2go::Client.new(options)
  end

  # Delegate to Invoice2go::Client
  def self.method_missing(method, *args, &block)
    return super unless client.respond_to?(method)
    client.send(method, *args, &block)
  end
end
