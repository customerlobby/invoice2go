module Invoice2go
  module Configuration

    VALID_OPTIONS_KEYS = [
      :api_key,
      :api_version,
      :adapter,
      :endpoint,
      :client_id,
      :client_secret,
      :grant_type,
      :refresh_token
    ].freeze

    # By default don't set the api key.
    DEFAULT_API_KEY = nil

    # By default use V1 of the API.
    DEFAULT_API_VERSION = ''.freeze

    # Use the Patron adapter as default tends to break on certain headers.
    DEFAULT_ADAPTER = Faraday::Adapter::Patron

    # By default use the main api URL.
    DEFAULT_ENDPOINT = 'https://api.2go.com'.freeze

    # By default request JSON data to be returned from the API.
    DEFAULT_FORMAT = :json

    attr_accessor *VALID_OPTIONS_KEYS

    # Convenience method to allow configuration options to be set in a block
    def configure
      yield self
    end

    def options
      VALID_OPTIONS_KEYS.inject({}) do |option, key|
        option.merge!(key => send(key))
      end
    end

    # When this module is extended, reset all settings.
    def self.extended(base)
      base.reset
    end

    # Reset all configuration settings to default values.
    def reset
      self.api_key     = DEFAULT_API_KEY
      self.api_version = DEFAULT_API_VERSION
      self.endpoint    = DEFAULT_ENDPOINT
      self.adapter     = DEFAULT_ADAPTER
    end
  end
end
