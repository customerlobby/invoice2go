require 'faraday_middleware'
require 'httpclient'
require 'patron'

Dir[File.expand_path('../../faraday/*.rb', __FILE__)].each{|f| require f}

module Invoice2go
  module Connection
    private

    def connection
      options = { url: endpoint }

      Faraday::Connection.new(options) do |connection|
        connection.use FaradayMiddleware::Invoice2goAuth, api_key
        connection.use FaradayMiddleware::Mashify
        connection.use Faraday::Response::RaiseError
        connection.request  :url_encoded
        connection.response :json, :content_type => 'application/json'
        connection.adapter :patron
      end
    end

    def connection_refresh
      Faraday::Connection.new('https://oauth.2go.com/identity/connect/token') do |connection|
        connection.use FaradayMiddleware::Mashify
        connection.request  :url_encoded
        connection.response :json, :content_type => /\bjson$/
        connection.adapter :patron
      end
    end
  end
end
