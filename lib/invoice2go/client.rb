module Invoice2go
  # Wrapper for the Invoice2go REST API.
  class Client < API
    Dir[File.expand_path('../client/*.rb', __FILE__)].each{|f| require f}

    include Invoice2go::Client::Accounts
    include Invoice2go::Client::Documents
    include Invoice2go::Client::Clients
  end
end
