module Invoice2go
  # Defines HTTP request methods
  module Request

    # Perform an HTTP GET request
    def get(path, options = {})
      request(:get, path, options)
    end

    # Perform an HTTP POST request
    def post(path, options = {})
      request(:post, path, options)
    end

    # Refresh auth token and save the new auth and refresh tokens
    def refresh_token!
      return "Error - missing or invalid refresh token, secret or client id. See the readme for details on how to configure your client." unless valid_params_for_refresh?
      response = connection_refresh.post '/identity/connect/token', params
      return update_config(response) if response.status == 200
      "Token could not be refreshed due to: #{response.body.error}"
    end

    private

    # Perform a HTTP request
    # Catch exception if token is expired, refresh token and retry.
    def request(method, path, options)
      begin
        response = connection.send(method) do |request|
          case method
          when :get
            formatted_options = format_options(options)
            request.url(path,formatted_options)
          when :post, :put
            request.body = options
            request.url(path)
          end
        end
        Response.create(response.body)
      rescue Faraday::ClientError => e
        if valid_params_for_refresh? && token_expired?(e.response)
          puts refresh_token!
        else
          e.response[:body]
        end
      end
    end

    def token_expired?(response)
      return unless response && response[:body]

      error = JSON.parse(response[:body])["Message"]
      error == "Bad Request: Access Token has expired"
    end

    def update_config(response)
      self.api_key = response.body.access_token
      self.refresh_token = response.body.refresh_token
    end

    def params
      {
          client_id: config[:client_id],
          client_secret: config[:client_secret],
          grant_type: "refresh_token",
          refresh_token: config[:refresh_token]
      }
    end

    def valid_params_for_refresh?
      config[:client_id] && config[:client_secret] && config[:refresh_token]
    end

    # Format the Options before you send them off to Invoice2go
    def format_options(options)
      return if options.blank?
      options[:document_type]     = options[:document_type] if options.has_key?(:document_type)
      options[:transaction_id]    = options[:transaction_id] if options.has_key?(:transaction_id)
      options[:from]              = options[:from] if options.has_key?(:from)
      options[:to]                = options[:to] if options.has_key?(:to)
      options[:page]              = options[:page]  if options.has_key?(:page)
      options[:page_size]         = options[:per_page]  if options.has_key?(:per_page)
      options[:fields]            = format_fields(options[:fields]) if options.has_key?(:fields)

      return options
    end

    # Format the fields to a format that the Invoice2go likes
    # @param [Array or String] fields can be specified as an Array or String
    # @return String
    def format_fields(fields)
      if fields.instance_of?(Array)
        return fields.join(",")
      elsif fields.instance_of?(String)
        return fields
      end
    end

  end
end
