module Invoice2go
  # Authentication middleware used if oauth_token and refresh_token are set
  class Token < Faraday::Middleware
    def params
      { grant_type: 'refresh_token',
        refresh_token: config[:refresh_token],
        client_id: config[:client_id],
        client_secret: config[:client_secret] }
    end
  end
end
