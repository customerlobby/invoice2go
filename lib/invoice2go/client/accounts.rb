module Invoice2go
  class Client
    module Accounts
      def accounts(params = {})
        get("accounts", params)
      end

      def account(id, params = {})
        get("accounts/#{id}", params)
      end
    end
  end
end
