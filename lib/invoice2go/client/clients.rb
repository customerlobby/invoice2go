module Invoice2go
  class Client
    module Clients
      def clients(id, params = {})
        get("accounts/#{id}/clients", params)
      end
    end
  end
end
