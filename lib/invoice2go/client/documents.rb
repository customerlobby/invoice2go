module Invoice2go
  class Client
    module Documents
      def documents(account_id, params = {})
        get("accounts/#{account_id}/documents", params)
      end

      def document(account_id, id, params = {})
        get("accounts/#{account_id}/documents/#{id}", params)
      end
    end
  end
end
