# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'invoice2go/version'

Gem::Specification.new do |spec|
  spec.name          = "invoice2go"
  spec.version       = Invoice2go::VERSION
  spec.authors       = ["Customer Lobby"]
  spec.email         = ["dev@customerlobby.com"]
  spec.description   = %q{Client for the Invoice2go API}
  spec.summary       = 'Client to interact with the Invoice2go API'
  spec.homepage      = "https://bitbucket.org/customerlobby/invoice2go"
  spec.license       = "MIT"

  spec.files         = `git ls-files`.split($/)
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.3"
  spec.add_development_dependency 'rake', '~> 10.4'
  spec.add_development_dependency('rspec', '~> 3.1')
  spec.add_development_dependency('webmock', '~> 1.6')
  spec.add_development_dependency('byebug', '~> 8.2')
  spec.add_runtime_dependency('hashie', '~> 3.4')
  spec.add_runtime_dependency('faraday', '~> 0.9')
  spec.add_runtime_dependency('faraday_middleware', '~> 0.10')
  spec.add_runtime_dependency('activesupport', '~> 4.2')
end
