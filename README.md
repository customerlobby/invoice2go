# Invoice2go

A Ruby wrapper for the Invoice2go REST API.

## Installation

Add this line to your application's Gemfile:

    gem 'invoice2go'

And then execute:

    $ bundle install

Or install it yourself as:

    $ gem install invoice2go

## Usage

### Configuration

Before you can make calls to Invoice2go you must configure the library with a valid API Token. You can request
a token be generated by Invoice2go. The API Token ties the API request to a particular Invoice2go company id.

There are two ways to configure the gem. You can pass a hash of configuration options when you create
a client, or you can use a configure block.

```ruby
client = Invoice2go.client({api_key: "YOUR_TOKEN_HERE"})
```

Middleware has been built in to automatically refresh the api_token when it expires if the details below are provided.

```ruby
Invoice2go.configure do |config|
  config.api_key = "YOUR_TOKEN_HERE"
  config.client_id = "YOUR_CLIENT_ID"
  config.client_secret = "YOUR_CLIENT_SECRET"
  config.refresh_token = "YOUR_REFRESH_TOKEN"
end

client = Invoice2go.client

You can manually refresh the api and refresh tokens by calling client.refresh_token!
```

```
There are only a limited number of calls available currently:

client.accounts
client.clients(account_id)
client.documents(account_id)

You can pass params to these calls as defined in the invoice2go api. i.e.

client.documents(account_id, {page: 1, page_size: 1000})
```